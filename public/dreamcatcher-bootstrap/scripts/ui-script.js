// Call our functions on window load event
window.onload = function() {
  setup(_default_data);
  $('#description_right').css('display', 'none');
  $('#radarChart').css('visibility', 'hidden');
  $('#questionPersona').css('display', 'none');
  $('.button_persona').css('display', 'none');
  $('#intro').css('display', 'none');
  $('.legend-container').css('display', 'none');
  $('.info-buttons').css('display', 'none');
}

function startExploration() {
  $('.button_persona').css('display', 'block');
  $('#questionPersona').css('display', 'block');
  $('#startExploration').css('display', 'none');
  $('#introText').css('display', 'block');
  $('.info-buttons').css('display', 'block');
  $('.openbtn').css('display', 'block');
  $('#intro').css('display', 'none');
}

function updatePersonaDescription(description, label, dreamerName, dreamerNamePronoun) {
  $('#radarChart').css('visibility', 'visible');
  $("#description").text(description);
  $("#label").text(label);
  $("#dreamerName").text(dreamerName);
  $("#dreamerNamePronoun").text(dreamerNamePronoun);
$('.legend-container').css('display', 'block');
$('#intro').css('display', 'none');
}

function updateDreamDescription(data) {
  if (!data) {
    $("#dream_text").text("");
    return;
  }
  $('#dream_text').html(data['text_dream']);
  $('#intro').css('display', 'inline-block');
  $('#legend').css('display', 'block');
}


  function openMethods() {
    document.getElementById("methods").style.width = "100%";
  }

  function openAbout() {
    document.getElementById("about").style.width = "100%";
  }

  function openHints() {
    document.getElementById("hints").style.width = "100%";
  }

  function closeMethods() {
    document.getElementById("methods").style.width = "0";
  }

  function closeAbout() {
    document.getElementById("about").style.width = "0";
  }

  function closeHints() {
    document.getElementById("hints").style.width = "0";
  }

  const PATTERNS = {
    start: {
      patternSpreadFactor: 0,
      patternShiftFactor: 0,
      patternStartFactor: 0
    },
    horseplayer: {
      patternSpreadFactor: 1.2,
      patternShiftFactor: 196.9970544332369,
      patternStartFactor: 0.1
    },
    artist: {
      patternSpreadFactor: 2.0,
      patternShiftFactor: 140.33127724323603,
      patternStartFactor: 0.1
    },
    blind: {
      patternSpreadFactor: 1.0,
      patternShiftFactor: 74.4093,
      patternStartFactor: 0.0
    },
    izzy: {
      patternSpreadFactor: 2.0,
      patternShiftFactor: 7.6917123523586755,
      patternStartFactor: 0.2
    },
    brides: {
      patternSpreadFactor: 1.5,
      patternShiftFactor: 632.5460507088995,
      patternStartFactor: 0.1
    },
    businessman: {
      patternSpreadFactor: 2.0,
      patternShiftFactor: 193.13986790807132,
      patternStartFactor: 0.3
    },
    warvet: {
      patternSpreadFactor: 1.5,
      patternShiftFactor: 102.81708342412371,
      patternStartFactor: 0.3
    },
  };
