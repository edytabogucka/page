// Main interactions for the webpage
// Jump to the visualization section once referencing button is clicked
function scrollTo() {
  $('a[href*=#]').on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: $($(this).attr('href')).offset().top}, 1000, 'linear');
  });
};

// Open and close sidebar
function openNav() {
  document.getElementById("mySidebar").style.width = "25vw";
  //document.getElementById("main").style.marginLeft = "0px";
}

function closeNav() {
  document.getElementById("mySidebar").style.width = "0";
  //document.getElementById("main").style.marginLeft= "0";
}
