$(function() {
  $('a[href*=#]').on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top}, 500, 'linear');
  });
});

function addMapFilter(e) {
    map.setFilter('streetname-occupation', ['match', ['get', 'occupation'], ['*'], true, false]);
    map.setFilter('streetname-gender', ['match', ['get', 'gender'], ['*'], true, false]);
    map.setFilter('streetname-foreigners', ['match', ['get', 'country_origin'], ['*'], true, false]);
    map.setFilter('streetname-streetsages', ['match', ['get', 'denomination'], ['*'], true, false]);
};
                    
function removeMapFilter(e) {
    map.setFilter('streetname-occupation', null);
    map.setFilter('streetname-gender', null);
    map.setFilter('streetname-foreigners', null);
    map.setFilter('streetname-streetsages', null);
};

