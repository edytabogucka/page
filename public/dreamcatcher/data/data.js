var data = [
				{ dreamer: 'dreamer',
					axes: [
                        {axis: 'Family members', value: 0},
                        {axis: 'Negative emotions', value: 0},
						{axis: 'Aggressive interactions', value: 0},
                        {axis: 'Animals', value: 100},
						{axis: 'Friends', value: 100},
                        {axis: 'Male characters', value: 100},
                        {axis: 'Female characters', value: 100},
						{axis: 'Imaginary beings', value: 0}
					], color: 'rgba(0,0,0,0)'
				}
			];

var _default_data = [
				{ dreamer: 'dreamer',
					axes: [
                        {axis: 'Family members', value: 0},
                        {axis: 'Negative emotions', value: 0},
						{axis: 'Aggressive interactions', value: 0},
                        {axis: 'Animals', value: 100},
						{axis: 'Friends', value: 100},
                        {axis: 'Male characters', value: 100},
                        {axis: 'Female characters', value: 100},
						{axis: 'Imaginary beings', value: 0}
					], color: 'rgba(0,0,0,0)'
				}
			];