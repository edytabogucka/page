// Create an element displaying the map.
// Specify the dimensions for the map container.
var width = 1300;
var height = 660;

// Select the map container and create a SVG element with dimensions as specified as above.
var svg = d3.select("#map")
  .append("svg")
  .attr("preserveAspectRatio", "xMidYMid") // preserve the aspect ratio
  .attr("viewBox", [0, 0, width, height]); // set the position and dimension, in user space, of an SVG viewport

// Create a new zoom behaviour to allow map zoom and pan
function zoomed({
  transform
}) {
  g.attr("transform", transform);
}

var zoom = d3.zoom()
  .extent([
    [0, 0],
    [width, height]
  ]) // set zoom extent the same as dimensions of the map container
  .scaleExtent([1, 10]) // set the scale range of zoom
  .on("zoom", zoomed);

svg
  .call(zoom) // assign zoom behaviour to the whole map container
  .style("pointer-events", "visiblePainted");

// Reset zoom button functionality
$("#reset").click(() => {
  svg.transition()
    .duration(500) // set transition time
    .call(zoom.transform, d3.zoomIdentity);
});

// Add titles to the button - small helpers explaining what will happen once button is pressed
$("#reset").attr('title', 'Reset view');
$("#search").attr('title', 'Search');

// Define the geographical projection for the map and set the initial zoom to show the map features.
var projection = d3.geoEquirectangular()
  .scale(3500) // the zoom level of
  // .rotate([-4.64,0]) //
  .center([7, 52.2]); // move down-up
//.translate([width/2, height/2]); // dragging the center of the map

// Prepare a path object and apply the projection to it.
var path = d3.geoPath()
  .projection(projection);

// Add a <g> group element to the SVG element which will group all thematic map layers (e.g. hexagons and borders).
// Assign it a class to style all maps elements later on.
var g = svg.append("g")
  .attr('class', 'map-features');

// Load the features from the GeoJSON to the <g> element
var hexagons = g.selectAll("path") // Specifics of D3 - select the (non-existing) path objects first
  .data(hexagons.features) // Enter the data from the Hexagons.geojson file
  .enter()
  .append("path") // For each feature, a <path> element is added
  .attr("d", path) // The "d" attribute defines data binded to the element and the successive coordinates of the points through which the path has to go
  .attr("class", "hexagons")
  .attr("fill", function(d, i) { // Initiate the first thematic map
    var colorScale = d3.scaleThreshold() // assign fixed classes based on the classification used in the paper
      .domain([0.37, 0.47, 0.54, 0.61, 1]) // set the ends of the classes
      .range(d3.schemeYlOrBr[5]); // set 5 classes
    return colorScale(d.properties.pcNSDAP333); // set the value field
  })
  .attr("stroke", "white")
  .attr("vector-effect", "non-scaling-stroke")
  .attr("stroke-width", 0.25);

// Set initial map and legend titles
d3.select("#mapTitle").text("Electoral results for the NSDAP")
d3.select("#legendTitle").text("Vote share of the results of the 1933 election")

// Set the visibility of legend containers
$('#legend-afd').hide();
$("#legend-nsdap").show();
$("#legend-expellees").hide();

// Add tooltip to be used to display municipality name
var div = d3.select("body")
  .append("div")
  .attr("class", "tooltip")
  .style("opacity", 0);

hexagons
  // assign on-click behaviour to the hexagons
  .on("click", function(event, d) {
    d3.select(this)
      .attr("z-index", "10")

    d.clicked = !d.clicked; // check if clicked

    let bounds = this.getBBox();
    let x0 = bounds.x;
    let x1 = bounds.x + bounds.width;
    let y0 = bounds.y;
    let y1 = bounds.y + bounds.height;
    //let scale = 1 / Math.max((x1 - x0) / width, (y1 - y0) / height); // zoom to the exact extent of the hexagon
    let scale = 10;
    // zoom and center map view around the clicked polygon
    g.transition()
      .duration(1500)
      .attr("transform", d.clicked ? "translate(" + (width / 2) + "," + (height / 2) + ") scale(" + scale + ") translate(" + (-(x0 + x1) / 2) + "," + (-(y0 + y1) / 2) + ")" : "transform(0,0) scale(1)");
  })
  // assign on-hover behaviour to the hexagons
  .on("mouseover", function(event, d) {
    //const e = hexagons.nodes();
    //const i = e.indexOf(this);
    //console.log(d.properties.Gemeindena);
    d3.select(this)
      .raise() // display the current hexagon on top of others
      .attr("class", "hexagons hover");
    div.transition()
      .duration(120) // time until tooltip appears
      .style("opacity", .9); // transparency of the tooltip
    div.html(d.properties.Gemeindena) // display the name of the municipality
      .style("font-size", "1.25rem")
      .style("left", (event.pageX + 10) + "px")
      .style("top", (event.pageY - 10) + "px");
  })
  .on("mouseout", function(d) {
    d3.select(this)
      .lower()
      .attr("class", "hexagons");
    div.transition()
      .duration(500)
      .style("opacity", 0);
  })

// Add data with cartogram borders
var borders = g.selectAll("borders")
  .data(borders.features)
  .enter()
  .append("path")
  .attr("d", path)
  .attr("stroke", "black")
  .attr("stroke-width", 1)
  .attr("fill", "none");

// Add data with state labels
var labels = g.selectAll("labels")
  .data(labels.features)
  .enter()
  .append("text")
  .attr("x", function(d) {
    return path.centroid(d)[0]
  })
  .attr("y", function(d) {
    return path.centroid(d)[1]
  })
  .attr("class", "labels")
  .text(function(d) {
    return d.properties.State
  })
  .attr("text-anchor", "middle")
  .attr("alignment-baseline", "central")
  .style("font-size", 10)
  .style("pointer-events", "none")
  .style("fill", "black");


// Define behaviour in the dropdown menu with indicators
var transitionTime = 1000;

// Dropdown change - NSDAP votes
d3.selectAll("#var-nsdap").on('click', function() {
  var colorScale = d3.scaleThreshold()
    .domain([0.37, 0.47, 0.54, 0.61, 1])
    .range(d3.schemeYlOrBr[5]);
  g.selectAll(".hexagons")
    .transition()
    .duration(transitionTime)
    .ease(d3.easeCubic)
    .attr("fill", function(d, i) {
      return colorScale(d.properties.pcNSDAP333);
    })
  d3.select("#mapTitle").text("Electoral results for the NSDAP");
  d3.select("#legendTitle").text("Vote share of the results of the 1933 election");

  $('#legend-afd').hide();
  $("#legend-nsdap").show();
  $("#legend-expellees").hide();
});

// Dropdown change - AfD votes
d3.selectAll("#var-afd").on('click', function() {
  var colorScale = d3.scaleThreshold()
    .domain([0.09, 0.12, 0.15, 0.2, 1])
    .range(d3.schemeBlues[5]);

  g.selectAll(".hexagons")
    .transition()
    .duration(transitionTime)
    .ease(d3.easeCubic)
    .attr("fill", function(d, i) {
      return colorScale(d.properties.pcAfD17);
    });

  d3.select("#mapTitle").text("Electoral results for the AfD");
  d3.select("#legendTitle").text("Vote share of the results of the 2017 federal election");

  $('#legend-afd').show();
  $("#legend-nsdap").hide();
  $("#legend-expellees").hide();
});

// Dropdown change - expellees
d3.selectAll("#var-expellees").on('click', function() {
  var colorScale = d3.scaleThreshold()
    .domain([0.07, 0.18, 0.25, 0.32, 1])
    .range(d3.schemeBuPu[5]);
  g.selectAll(".hexagons")
    .transition()
    .duration(transitionTime)
    .ease(d3.easeCubic)
    .attr("fill", function(d, i) {
      return colorScale(d.properties.share_expe);
    });
  d3.select("#mapTitle").text("German expellees in 1950");
  d3.select("#legendTitle").text("Share of expellees in the total population");

  $('#legend-afd').hide();
  $("#legend-nsdap").hide();
  $("#legend-expellees").show();

});

// Functionalities of search bar

// Create an empty array to store the names of municipalities
var list = [];

d3.selectAll(".hexagons")
  .filter(function(d) {
    list.push(d.properties.Gemeindena); // append the names of the municipalities to the array
  });

// Add the names to the selection list
d3.select('datalist').selectAll('option')
  .data(list) // performing a data join
  .enter() // extracting the entering selection
  .append('option') // adding an option to the selection of municipalities
  .attr('value', function (d) { return d}); // add attribute with municipality name

// Set what will happen after entering the municipality name
d3.select("#search").on("click", function(d, i) {
  var txtName = d3.select("#txtName")
    .node().value; // get the name entered in the search bar
  var filtered = d3.selectAll(".hexagons")
    .filter(function(d) {
      return d.properties.Gemeindena === txtName; // check if input name matches the name in the dataset
    })
    .raise() // display the current hexagon on top of others
    .attr("vector-effect", "non-scaling-stroke")
    .attr("stroke", "black")
    .attr("stroke-width", 1.5);

  // console.log(filtered.node()); // returns the path of the selected hexagon
  d.clicked = !d.clicked;

  var bounds = filtered.node().getBBox();
  var x0 = bounds.x;
  var x1 = bounds.x + bounds.width;
  var y0 = bounds.y;
  var y1 = bounds.y + bounds.height;
  var scale = 40;
  // Zoom to the municiplaity found in the search
  g.transition()
    .duration(1500)
    .attr("transform", d.clicked ? "translate(" + (width / 2) + "," + (height / 2) + ") scale(" + scale + ") translate(" + (-(x0 + x1) / 2) + "," + (-(y0 + y1) / 2) + ")" : "transform(0,0) scale(1)");

});

// Legend construction - set the size of legend symbol
var size = 20

// Legend change - NSDAP
var keys_nsdap = ["0.00 - 0.37", "0.38 - 0.47", "0.48 - 0.54", "0.55 - 0.61", "0.62 - 1"];
var color_nsdap = d3.scaleOrdinal()
  .domain(keys_nsdap)
  .range(d3.schemeYlOrBr[5])

var legend_nsdap = d3.select("#legend-nsdap")

legend_nsdap.selectAll("dots")
  .data(keys_nsdap)
  .enter()
  .append("rect")
  .attr("x", 0)
  .attr("y", function(d, i) {
    return 0 + i * (size + 5)
  })
  .attr("width", size)
  .attr("height", size)
  .style("fill", function(d) {
    return color_nsdap(d)
  })

legend_nsdap.selectAll("labels")
  .data(keys_nsdap)
  .enter()
  .append("text")
  .attr("x", 10 + size * 1.2)
  .attr("y", function(d, i) {
    return 0 + i * (size + 5) + (size / 2)
  })
  .style("fill", "black")
  .text(function(d) {
    return d
  })
  .attr("text-anchor", "left")
  .style("alignment-baseline", "middle")
  .style("font-size", "1rem");

// Legend change - AfD
var keys_afd = ["0.00 - 0.09", "0.10 - 0.12", "0.13 - 0.15", "0.16 - 0.20", "0.21 - 0.50"];
var color_afd = d3.scaleOrdinal()
  .domain(keys_afd)
  .range(d3.schemeBlues[5])

var legend_afd = d3.select("#legend-afd")

legend_afd.selectAll("dots")
  .data(keys_afd)
  .enter()
  .append("rect")
  .attr("x", 0)
  .attr("y", function(d, i) {
    return 0 + i * (size + 5)
  })
  .attr("width", size)
  .attr("height", size)
  .style("fill", function(d) {
    return color_afd(d)
  })

legend_afd.selectAll("labels")
  .data(keys_afd)
  .enter()
  .append("text")
  .attr("x", 10 + size * 1.2)
  .attr("y", function(d, i) {
    return 0 + i * (size + 5) + (size / 2)
  })
  .style("fill", "black")
  .text(function(d) {
    return d
  })
  .attr("text-anchor", "left")
  .style("alignment-baseline", "middle")
  .style("font-size", "1rem");


  // Legend change - exellees
  var keys_expellees = ["0.02 - 0.07", "0.08 - 0.18", "0.19 - 0.25", "0.26 - 0.32", "0.33 - 0.54"];
  var color_expellees = d3.scaleOrdinal()
    .domain(keys_expellees)
    .range(d3.schemeBuPu[5]);

  var legend_expellees = d3.select("#legend-expellees")

  legend_expellees.selectAll("dots")
    .data(keys_expellees)
    .enter()
    .append("rect")
    .attr("x", 0)
    .attr("y", function(d, i) {
      return 0 + i * (size + 5)
    })
    .attr("width", size)
    .attr("height", size)
    .style("fill", function(d) {
      return color_expellees(d)
    })

  legend_expellees.selectAll("labels")
    .data(keys_expellees)
    .enter()
    .append("text")
    .attr("x", 10 + size * 1.2)
    .attr("y", function(d, i) {
      return 0 + i * (size + 5) + (size / 2)
    })
    .style("fill", "black")
    .text(function(d) {
      return d
    })
    .attr("text-anchor", "left")
    .style("alignment-baseline", "middle")
    .style("font-size", "1rem");
