// Main interactions for the webpage
// Jump to the visualization section once referencing button is clicked
$(function() {
  $('a[href*=#]').on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top}, 500, 'linear');
  });
});

// Set the preloader for the cover page
jQuery(document).ready(function ($) {
    $(window).load(function () {
        setTimeout(function(){
            $('#preloader').fadeOut('slow', function () {
            });
        },1800);
    });
});


// Generate particles
function range(start, end) {
  return Array(end - start + 1).fill().map((_, idx) => start + idx)
}
var particleNumber = range(1, 50);

window.onload = function() {
  $("#section03").hide();
  $("body").addClass('all-loaded');
  $("#theme").text("Select occupations"); // change the title of the map to occupation
};

// Scroll top on website reload
history.scrollRestoration = "manual";
  $(window).on('beforeunload', function(){
  $(window).scrollTop(0);
});

// Behaviour of map layers when user "toggles off" all categorical labels
// Setting map filter to all records - does not show any street
function addMapFilter(e) {
    map.setFilter('streetname-occupation', ['match', ['get', 'occupation'], ['*'], true, false]);
    map.setFilter('streetname-gender', ['match', ['get', 'gender'], ['*'], true, false]);
    map.setFilter('streetname-foreigners', ['match', ['get', 'country_origin'], ['*'], true, false]);
    map.setFilter('streetname-streetsages', ['match', ['get', 'denomination'], ['*'], true, false]);
};

// Behaviour of map layers when user "toggles on" all categorical labels
// Setting map filter to null - shows all streets
function removeMapFilter(e) {
    map.setFilter('streetname-occupation', null);
    map.setFilter('streetname-gender', null);
    map.setFilter('streetname-foreigners', null);
    map.setFilter('streetname-streetsages', null);
};

// Unselecting single street clicked by the user
// Removes red color indicating the selected street
function removeSelection(e) {
  if (typeof map.getLayer('selectedStreet') !== "undefined") {
    map.removeLayer('selectedStreet')
    map.removeSource('selectedStreet');
  }
};

// Changes the value of the toggle to "Hide all"
function toggleOff() {
  $('#toggle').bootstrapToggle('off');
};

function dropdownUI() {
  $("#showStreetStory").css('visibility', 'visible');
  $('#legend').css('visibility', 'visible');
  toggleOff();
  removeSelection();
  if($('.mapboxgl-popup').length){
	popup.remove();
  };
  $('input').prop('checked', true);
  removeMapFilter();
  themeFilter = undefined;
  $('#slider').val(2020);
  $('#active-year').text("2020");
};
