// Add tooltip to be used to display data-driven information
var div = d3.select("body") // Select the body of the website
  .append("div") // Add the container for the tooltip content
  .attr("class", "tooltip") // Add class name to the container
  .style("opacity", 0); // Set the initial transparency of tooltip to 0 - unvisible

let labelContainer = d3.select('#dna')
  .append("div")
  .attr("id", "label-container");

var labels = ["Farmland", "Orchards", "Residential areas", "Forest", "Industrial zones", "Roads and transport", "Roofs"];

for (let i = 0; i < labels.length; i++) {
  let selector = labels[i].split(' ')[0];
  let alias = labels[i]

  // Append checkbox
  let label = d3.select("#label-container")
    .append("label")
    .attr("class", "label")
    .attr("id", "label-" + selector.toLowerCase())

  label
    .append("input")
    .attr("type", "checkbox")

  label
    .append("span")
    .attr("id", selector.toLowerCase())
    .attr("class", "checkbox")
    .text(alias)

  // select all stripe sections with class name
  let selected = d3.selectAll("." + selector.toLowerCase())

  // select all legend entries with id
  let selectedLegend = d3.select("#" + selector.toLowerCase())
    //console.log(selectedLegend)
    .on("click", function() {
      //console.log("Test")
      var active = selected.active ? false : true;
      highlight = active ? true : false;
      d3.selectAll(".section")
        .classed("section-selected", highlight)
      d3.selectAll("." + selector.toLowerCase())
        .classed("category-selected", highlight)
      selected.active = active;
    })
}

var cities = ["Florence", "Torino", "Palermo", "Bologna", "Milan", "Rome"];

for (let i = 0; i < cities.length; i++) {

  let selector = cities[i];

  let wrapper = d3.select('#dna')
    .append("div")
    .attr("id", function(d) {
      return "wrapper-" + selector.toLowerCase()
    })
    .attr("class", "wrapper");

  let name = wrapper
    .append("div")
    .attr("class", "city-name")
    .append("p")
    .text(selector);

  let container = wrapper
    .append("div")
    .attr("id", function(d) {
      return "container-" + selector.toLowerCase()
    })
    .attr("class", "container");


  d3.csv("data/data.csv").then(function(data) {
    container
      .selectAll('sections')
      .data(data
        .filter(function(d) {
          return d.city == selector
        }))
      .enter()

      //.append("g")
      .append("div")
      .attr("id", function(d) {
        return d.city.toLowerCase() + "-" + d.sortedID
      })
      .attr("class", function(d) {
        return "section " + d.lu_class.toLowerCase()
      })
      //.style("opacity", 1)
      //.style('stroke', "black")
      .style("stroke-width", 0.5)
      .style('width', function(d) {
        return (d.width)
      })
      //.style('background', "teal")
      .on("mouseover", function(event, d) {
        d3.select(this) // Get the element which is being hovered over
        div.transition() // Set time until styling effect appears
          .duration(500)
          .style("opacity", 1); // Set the transparency of the tooltip
        div.html('<div>' + '<img src= ' + d.path + '>' + '</div>' +  '<br>' + '<div class="story">' +'<b>' +  d.lu_class + '</b>' + " in " + d.city + " " + d.story + '</div>') // Display the data-driven text in the tooltip
          .style("left", (event.pageX + 10) + "px") // Horizontal position of the tooltip - horizontal distance from the mouse pointer
          .style("top", (event.pageY + 10) + "px"); // Vertical position of the tooltip - vertical distance from the mouse pointer
      })

      .on("mouseout", function(d) {
        d3.select(this)
        div.transition()
          .duration(500) // Set time until tooltip disappears
          .style("opacity", 0); // Hide the tooltip - reset the initial transparency of tooltip to 0 - unvisible
      })
  });
};







// Determine if nightmaree is visible
// https://jaketrent.com/post/d3-class-operations
// select all stripe sections with class name
//let greenery = d3.selectAll(".greenery")
//let greenery_legend = d3.select("#greenery")
//  .on("click", function() {
//    var active = greenery.active ? false : true;

//    // newOpacity = active ? 0.7 : 1;
//    newClass = active ? true : false;
//newColor = active ? "black" : "black";
//    d3.selectAll(".section")
//      .classed("section-selected", newClass)
//    d3.selectAll(".greenery")
//      .classed("category-selected", newClass)
// Update whether or not the elements are active
//    greenery.active = active;
//  });
