var swatchesContainer = d3.select('#palettes')
  .append("div")
  .attr("id", "swatches-container")
  .attr("class", "swatches-container");

var swatches = ["Roofs",  "Greenery"];

for (let s = 0; s < swatches.length; s++) {

  let selector = swatches[s].split(' ')[0];
  let alias = swatches[s];

  d3.csv("data/swatches.csv").then(function(data) {
    var selectedContainer = d3.select('#swatches-container')
      .append("div")
      .attr("id", "swatches-container-" + selector.toLowerCase())
      .attr("class", "swatch-row")

    selectedContainer
      .append("p")
      .attr("class","swatch-category")
      .text(alias);

    selectedContainer
      .selectAll('swatches')
      .data(data
        .filter(function(d) {
          return d.class == selector
        }))
      .enter()
      .append("div")
      .attr("class", "swatch")
      .style("background-image", function(d) {
        return "url(" + d.path + ")";
      })
      .style("background-color", function(d) {
        return d.color;
      })
      /*
      .attr("filter", "contrast(1)") */

      .on("mouseover", function(event, d) {
        d3.select(this)
          .transition()
          .duration(500)
          .style("background-color", "transparent")
      })
      .on("mouseout", function(event, d) {
        d3.select(this)
          .transition()
          .duration(500)
          .style("background-color", function(d) {
            return d.color;
          })
      })
      /*
      .on("mouseout", function(event, d) {
        d3.select(this)
          .style("background", function(d) {
            return d.color;
          })
      })
      */
      .append("div")
      .attr("class", "caption")
      .text(function(d) {
        return d.city;
      });
    //.attr("id", "swatch-" + selector.toLowerCase())
  });
}

/*
function tabulate(data, columns) {
  var wrapper = d3.select('body')
    .append("div")
    .attr("class", "swatches");
  var table = wrapper
    .append('table')
    .attr("class", "wrapper");
  var thead = table.append('thead')
  var tbody = table.append('tbody');

  // append the header row
  thead.append('tr')
    .selectAll('th')
    .data(columns).enter()
    .append('th')
    .text(function(column) {
      return column;
    });

  // create a row for each object in the data
  var rows = tbody.selectAll('tr')
    .data(data)
    .enter()
    .append('tr');

  // create a cell in each row for each column
  var cells = rows.selectAll('td')
    .data(function(row) {
      return columns.map(function(column) {
        return {
          column: column,
          value: row[column]
        };
      });
    })
    .enter()
    .append('td')
    .text(function(d) {
      return d.value;
    });

  return table;
}

d3.csv("data/swatches.csv").then(function(data) {
  var columns = ['lu_class', 'Bologna', 'Rome', 'Florence', "Milano", "Palermo", "Torino"]
  tabulate(data, columns);
});
*/
