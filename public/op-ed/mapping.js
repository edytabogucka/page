// Call functions on window load event
window.onload = function() {
  drawChoropleth()
}

// *** SVG MAP CONTAINER *** //
// Create an element displaying the map.
// Specify the dimensions for the map container.
var width = 1300;
var height = 660;

// Select the map container and create a SVG element with dimensions as specified as above.
var svg = d3.select("#map")
  .append("svg")
  //.style("cursor", "grab")
  .attr("preserveAspectRatio", "xMidYMid") // preserve the aspect ratio
  .attr("viewBox", [0, 0, width, height]); // set the position and dimension, in user space, of an SVG viewport

// Create a new zoom behaviour to allow map zoom and pan
function zoomed({
  transform
}) {
  g.attr("transform", transform);
}

var zoom = d3.zoom()
  .extent([
    [0, 0],
    [width, height]
  ]) // set zoom extent the same as dimensions of the map container
  .scaleExtent([1, 10]) // set the scale range of zoom
  .on("zoom", zoomed);
svg
  .call(zoom) // assign zoom behaviour to the whole map container
  .style("pointer-events", "visiblePainted");

// Reset zoom button functionality
$("#reset").click(() => {
  svg.transition()
    .duration(500) // set transition time
    .call(zoom.transform, d3.zoomIdentity);
  d3.selectAll(".labels")
    .style("font-size", 10)
});

// Add a <g> group element to the SVG element which will group all thematic map layers, e.g. hexagons and choropleth municipalities.
// Assign it a class to style all maps elements later on.
var g = svg.append("g")
  .attr('class', 'map-features');

// Add a <g> group element to the SVG element which will group all hexagons.
var h = g.append("g")
  .attr('class', 'map-features-hexagons');

// Add a <g> group element to the SVG element which will group all choropleth municipalities.
var l = g.append("g")
  .attr('class', 'map-features-municipalities');

var t = g.append("g")
  //.attr("transform", "rotate(-6)")
  .attr('class', 'text-municipalities');

// *** DRAW HEXAGONAL MAP *** //
function drawHexagons() {
  d3.selectAll(".map-features-municipalities")
    .attr("visibility", "hidden"); // Switch off the choropleth municipalities
  d3.selectAll(".text-municipalities")
    .attr("visibility", "hidden"); // Switch off the choropleth municipalities
  d3.selectAll(".map-features-hexagons")
    .attr("visibility", "visible"); // switch on the hexagons

  document.getElementById("reset").click(); // Reset map view
  d3.select(".explanation")
    .style("display", "block");

  // Update the search bar
  // Set what will happen after entering the municipality name
  d3.select("#search").on("click", function(d, i) {
    var txtName = d3.select("#txtName")
      .node().value; // get the name entered in the search bar
    var filtered = d3.selectAll(".hexagons") // Browse through all hexagons
      .filter(function(d) {
        return d.properties.Gemeindename === txtName; // check if input name matches the name in the dataset
      })
      .raise() // display the current hexagon on top of others
      .attr("vector-effect", "non-scaling-stroke")
      .attr("stroke", "black")
      .attr("stroke-width", 1.5);

    // console.log(filtered.node()); // returns the path of the selected hexagon
    d.clicked = !d.clicked;

    var bounds = filtered.node().getBBox();
    var x0 = bounds.x;
    var x1 = bounds.x + bounds.width;
    var y0 = bounds.y;
    var y1 = bounds.y + bounds.height;
    var scale = 40;
    // Zoom to the hexagon found in the search
    g.transition()
      .duration(1500)
      .attr("transform", d.clicked ? "translate(" + (width / 2) + "," + (height / 2) + ") scale(" + scale + ") translate(" + (-(x0 + x1) / 2) + "," + (-(y0 + y1) / 2) + ")" : "transform(0,0) scale(1)");
  });

}
// *** DRAW CHOROPLETH MAP *** //
function drawChoropleth() {
  d3.selectAll(".map-features-hexagons")
    .attr("visibility", "hidden"); // Swicth off the hexagons
  d3.selectAll(".map-features-municipalities")
    .attr("visibility", "visible"); // Swicth on the choropleth municipalities
  d3.selectAll(".text-municipalities")
    .attr("visibility", "visible"); // Switch off the choropleth municipalities
  document.getElementById("reset").click(); // Reset the map view
  d3.select(".explanation")
    .style("display", "none");

  // Set what will happen after entering the municipality name
  d3.select("#search").on("click", function(d, i) {
    var txtName = d3.select("#txtName")
      .node().value; // get the name entered in the search bar
    var filtered = d3.selectAll(".municipalities")
      .filter(function(d) {
        return d.properties.Gemeindename === txtName; // check if input name matches the name in the dataset
      })
      .raise() // display the current hexagon on top of others
      .attr("vector-effect", "non-scaling-stroke")
      .attr("stroke", "black")
      .attr("stroke-width", 1.5);

    // console.log(filtered.node()); // returns the path of the selected municipality
    d.clicked = !d.clicked;

    var bounds = filtered.node().getBBox();
    var x0 = bounds.x;
    var x1 = bounds.x + bounds.width;
    var y0 = bounds.y;
    var y1 = bounds.y + bounds.height;
    var scale = (1 / Math.max((x1 - x0) / width, (y1 - y0) / height)) - 20
    // Zoom to the municipality found in the search
    g.transition()
      .duration(1500)
      .attr("transform", d.clicked ? "translate(" + (width / 2) + "," + (height / 2) + ") scale(" + scale + ") translate(" + (-(x0 + x1) / 2) + "," + (-(y0 + y1) / 2) + ") rotate(-6)" : "transform(0,0) scale(1)");
  });
}


// *** Assign the map drawing functions to buttons *** //
let choropleth = d3.select("#chorButton")
  .on("click", drawChoropleth);

let hexmap = d3.select("#hexButton")
  .on("click", drawHexagons);

//*** DROPDOWN WITH THEMATIC LAYERS *** //
// Define behaviour in the dropdown menu with indicators
var transitionTime = 1000;

// Dropdown change - NSDAP votes in 1933 //
d3.selectAll("#var-nsdap").on('click', function() {
  var colorScale = d3.scaleThreshold() // assign fixed classes based on the classification used in the paper
    .domain([0.37, 0.47, 0.54, 0.61, 1]) // set the ends of the classes
    //.range(d3.schemeYlOrBr[5]); // set 5 classes
    .range(['rgb(255, 237, 160)', 'rgb(254, 178, 76)', 'rgb(252,78,42)', 'rgb(189,0,38)', "rgb(112,0,28)"]) // set 5 classes

  g.selectAll(".hexagons")
    .transition()
    .duration(transitionTime)
    .ease(d3.easeCubic)
    .attr("fill", function(d, i) {
      if (d.properties.NSDAP33 == "keine Daten") {
        return "white";
      } else {
        return colorScale(d.properties.NSDAP33);
      }
    });

  g.selectAll(".municipalities")
    .transition()
    .duration(transitionTime)
    .ease(d3.easeCubic)
    .attr("fill", function(d, i) {
      if (d.properties.NSDAP33 == "keine Daten") {
        return "white";
      } else {
        return colorScale(d.properties.NSDAP33);
      }
    });

  d3.select("#mapTitle").text("Wahlergebnisse der NSDAP - 1933");
  d3.select("#legendTitle").text("Wahlergebnisse der NSDAP - 1933");

  $('#legend-afd').hide();
  $('#legend-afd-21').hide();
  $("#legend-nsdap").show();
});

// Dropdown change - AfD votes in 2017 //
d3.selectAll("#var-afd").on('click', function() {
  var colorScale = d3.scaleThreshold()
    .domain([0.09, 0.12, 0.15, 0.2, 1])
    .range(['rgb(239, 243, 255)', 'rgb(189, 215, 231)', 'rgb(107, 174, 214)', 'rgb(49, 130, 189)', "rgb(8, 81, 156)"])

  g.selectAll(".hexagons")
    .transition()
    .duration(transitionTime)
    .ease(d3.easeCubic)
    .attr("fill", function(d, i) {
      if (d.properties.AfD17 == "keine Daten") {
        return "white";
      } else {
        return colorScale(d.properties.AfD17);
      }
    });

  g.selectAll(".municipalities")
    .transition()
    .duration(transitionTime)
    .ease(d3.easeCubic)
    .attr("fill", function(d, i) {
      if (d.properties.AfD17 == "keine Daten") {
        return "white";
      } else {
        return colorScale(d.properties.AfD17);
      }
    });


  d3.select("#mapTitle").text("Wahlergebnisse der AfD in 2017");
  d3.select("#legendTitle").text("Wahlergebnisse der AfD in 2017");

  $('#legend-afd').show();
  $("#legend-nsdap").hide();
  $('#legend-afd-21').hide();
});

// Dropdown change - AfD votes in 2021 //
d3.selectAll("#var-afd-21").on('click', function() {
  var colorScale = d3.scaleThreshold()
    .domain([7.7, 9.9, 13, 24, 100])
    .range(['rgb(239, 243, 255)', 'rgb(189, 215, 231)', 'rgb(107, 174, 214)', 'rgb(49, 130, 189)', "rgb(8, 81, 156)"])

  g.selectAll(".hexagons")
    .transition()
    .duration(transitionTime)
    .ease(d3.easeCubic)
    .attr("fill", function(d, i) {
      if (d.properties.AfD21 == "keine Daten") {
        return "white";
      } else {
        return colorScale(d.properties.AfD21);
      }
    });

  g.selectAll(".municipalities")
    .transition()
    .duration(transitionTime)
    .ease(d3.easeCubic)
    .attr("fill", function(d, i) {
      if (d.properties.AfD21 == "keine Daten") {
        return "white";
      } else {
        return colorScale(d.properties.AfD21);
      }
    });

  d3.select("#mapTitle").text("Wahlergebnisse der AfD in 2021");
  d3.select("#legendTitle").text("Wahlergebnisse der AfD in 2021");

  $('#legend-afd-21').show();
  $("#legend-nsdap").hide();
  $('#legend-afd').hide();
});

// *** SEARCH BAR *** //
// Add titles to the button - small helpers explaining what will happen once button is pressed
$("#reset").attr('title', 'Ansicht zurücksetzen');
$("#search").attr('title', 'Suche');

// Trigger data search and zoom once user presses enter/return
// Get the input field
var input = document.getElementById("txtName");
// Execute a function when the user releases a key on the keyboard
input.addEventListener("keyup", function(event) {
  // Number 13 is the "Enter" key on the keyboard
  if (event.keyCode === 13) {
    // Cancel the default action, if needed
    event.preventDefault();
    // Trigger the button element with a click
    document.getElementById("search").click();
  }
});

// *** MAP LEGEND *** //
// Set initial map and legend titles
d3.select("#mapTitle").text("Wahlergebnisse der NSDAP in 1933")
d3.select("#legendTitle").text("Wahlergebnisse der NSDAP in 1933")

// Set the visibility of legend containers
$("#legend-nsdap").show();
$('#legend-afd').hide();
$('#legend-afd-21').hide();

// Add tooltip to be used to display additional data
var div = d3.select("body")
  .append("div")
  .attr("class", "tooltip")
  .style("opacity", 0);

// Legend construction - set the size of legend symbol
var size = 20

// Legend change - NSDAP 1933
var keys_nsdap = ["keine Daten", "0 - 37%", "38 - 47%", "48 - 54%", "55 - 61%", "62 - 94%"];
var color_nsdap = d3.scaleOrdinal()
  .domain(keys_nsdap)
  .range(['white', 'rgb(255, 237, 160)', 'rgb(254, 178, 76)', 'rgb(252,78,42)', 'rgb(189,0,38)', "rgb(112,0,28)"]) // set 6 classes

var legend_nsdap = d3.select("#legend-nsdap")

legend_nsdap.selectAll("dots")
  .data(keys_nsdap)
  .enter()
  .append("rect")
  .attr("x", 0)
  .attr("y", function(d, i) {
    return 0 + i * (size + 5)
  })
  .attr("width", size)
  .attr("height", size)
  .style("fill", function(d) {
    return color_nsdap(d)
  })
  .style("stroke-width", "0.5px")
  .style("stroke", "black")

legend_nsdap.selectAll("labels")
  .data(keys_nsdap)
  .enter()
  .append("text")
  .attr("x", 10 + size * 1.2)
  .attr("y", function(d, i) {
    return 0 + i * (size + 5) + (size / 2)
  })
  .style("fill", "black")
  .text(function(d) {
    return d
  })
  .attr("text-anchor", "left")
  .style("alignment-baseline", "middle")
  .style("font-size", "1rem");

// Legend change - AfD 2017
var keys_afd = ["keine Daten", "1 - 9%", "10 - 12%", "13 - 15%", "16 - 20%", "21 - 50%"];
var color_afd = d3.scaleOrdinal()
  .domain(keys_afd)
  .range(['white', 'rgb(239, 243, 255)', 'rgb(189, 215, 231)', 'rgb(107, 174, 214)', 'rgb(49, 130, 189)', "rgb(8, 81, 156)"])

var legend_afd = d3.select("#legend-afd")

legend_afd.selectAll("dots")
  .data(keys_afd)
  .enter()
  .append("rect")
  .attr("x", 0)
  .attr("y", function(d, i) {
    return 0 + i * (size + 5)
  })
  .attr("width", size)
  .attr("height", size)
  .style("fill", function(d) {
    return color_afd(d)
  })
  .style("stroke-width", "0.5px")
  .style("stroke", "black")

legend_afd.selectAll("labels")
  .data(keys_afd)
  .enter()
  .append("text")
  .attr("x", 10 + size * 1.2)
  .attr("y", function(d, i) {
    return 0 + i * (size + 5) + (size / 2)
  })
  .style("fill", "black")
  .text(function(d) {
    return d
  })
  .attr("text-anchor", "left")
  .style("alignment-baseline", "middle")
  .style("font-size", "1rem");

// Legend change - AfD 2021
var keys_afd_21 = ["keine Daten", "0 - 7.7%", "7.8 - 9.9%", "10 - 13%", "14 - 24%", "25 - 56%"];
var color_afd_21 = d3.scaleOrdinal()
  .domain(keys_afd_21)
  .range(['white', 'rgb(239, 243, 255)', 'rgb(189, 215, 231)', 'rgb(107, 174, 214)', 'rgb(49, 130, 189)', "rgb(8, 81, 156)"])

var legend_afd_21 = d3.select("#legend-afd-21")

legend_afd_21.selectAll("dots")
  .data(keys_afd_21)
  .enter()
  .append("rect")
  .attr("x", 0)
  .attr("y", function(d, i) {
    return 0 + i * (size + 5)
  })
  .attr("width", size)
  .attr("height", size)
  .style("fill", function(d) {
    return color_afd_21(d)
  })
  .style("stroke-width", "0.5px")
  .style("stroke", "black")

legend_afd_21.selectAll("labels")
  .data(keys_afd_21)
  .enter()
  .append("text")
  .attr("x", 10 + size * 1.2)
  .attr("y", function(d, i) {
    return 0 + i * (size + 5) + (size / 2)
  })
  .style("fill", "black")
  .text(function(d) {
    return d
  })
  .attr("text-anchor", "left")
  .style("alignment-baseline", "middle")
  .style("font-size", "1rem");
