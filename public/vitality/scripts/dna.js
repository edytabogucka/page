var animation = d3.select('#animation')
  .append("svg")
  .attr("id", "dataviz_delay")
  .attr("preserveAspectRatio", "xMidYMid") // Preserve the aspect ratio
  .attr("viewBox", [0, 0, 300, 150]);

function drawGradient() {
  //console.log("we will draw")
  const svg = d3.select('#dataviz_delay')
    .append("g")


  svg
    .append("text")
    .attr("class", "animation-text-left")
    .attr("x", 25)
    .attr("y", 65)
    .attr("font-size", "6px")
    .style("fill", "none")
    .text("lower vitality");


  svg
    .append("text")
    .attr("x", 245)
    .attr("y", 65)
    .attr("class", "animation-text-right")
    .attr("font-size", "6px")
    .style("fill", "none")
    .text("higher vitality");

  var line = svg.append("path")
    .attr("d", "M0,81.621c161.387,0,243.466-20.325,274.621-33.621  c26.166-11.167,44.833-35.571,44.833-38.75")
    .attr("stroke", "black")
    .attr("transform", "scale(0.77),translate(33,-10)")
    .attr("stroke-width", 0.5)
    .attr("id", "gradient-path")
    .style("opacity", 1)
    .attr("fill", "none");

  const length = line.node().getTotalLength();
  //console.log(length)

  line
    .attr("stroke-dasharray", length + " " + length)
    .attr("stroke-dashoffset", length);


  d3
    .select("#gradient")
    .append(() => svg.node());
}

drawGradient();

function animateGradient() {
  // Append label to the gradient


  d3.selectAll(".animation-text-left")
    .transition()
    .delay(1000)
    .style("fill", "black")
    .duration(2000)

  d3.selectAll(".animation-text-right")
      .transition()
      .delay(3000)
      .style("fill", "black")
  // Animate gradient line
  d3.select("#gradient-path")
    .transition()
    .delay(1000)
    .ease("linear")
    .attr("stroke-dashoffset", 0)
    .duration(2000)
}


var position = [{
    // row 1
    "x": 0,
    "y": 0,
    "sorted": 7,
    "color": "rgb(163,168,188)",
    "path" : "img/tooltip/milan/milan-9025.jpg",
    "lu_class" : "Test"
  },
  {
    "x": 1,
    "y": 0,
    "sorted": 8,
    "color": "rgb(187,208,225)"
  },
  {
    "x": 2,
    "y": 0,
    "sorted": 10,
    "color": "rgb(194,209,228)"
  },
  {
    "x": 3,
    "y": 0,
    "sorted": 9,
    "color": "rgb(198,217,255)"
  },
  {
    "x": 4,
    "y": 0,
    "sorted": 6,
    "color": "rgb(158,166,151)"
  },
  // row 2
  {
    "x": 0,
    "y": 1,
    "sorted": 17,
    "color": "rgb(162,129,136)"
  },
  {
    "x": 1,
    "y": 1,
    "sorted": 11,
    "color": "rgb(170,184,213)"
  },

  {
    "x": 2,
    "y": 1,
    "sorted": 23,
    "color": "rgb(150,102,98)"
  },
  {
    "x": 3,
    "y": 1,
    "sorted": 19,
    "color": "rgb(185,124,132)"
  },
  {
    "x": 4,
    "y": 1,
    "sorted": 13,
    "color": "rgb(185,146,150)"
  },


  // row 3
  {
    "x": 0,
    "y": 2,
    "sorted": 18,
    "color": "rgb(79,46,39)"
  },
  {
    "x": 1,
    "y": 2,
    "sorted": 21,
    "color": "rgb(187,104,122)"
  },

  {
    "x": 2,
    "y": 2,
    "sorted": 22,
    "color": "rgb(171,99,100)"
  },
  {
    "x": 3,
    "y": 2,
    "sorted": 24,
    "color": "rgb(191,126,134)"
  },
  {
    "x": 4,
    "y": 2,
    "sorted": 14,
    "color": "rgb(239,185,185)"
  },

  // row 4
  {
    "x": 0,
    "y": 3,
    "sorted": 4,
    "color": "rgb(84,99,76)"
  },

  {
    "x": 1,
    "y": 3,
    "sorted": 5,
    "color": "rgb(139,137,120)"
  },

  {
    "x": 2,
    "y": 3,
    "sorted": 20,
    "color": "rgb(195,149,151)"
  },

  {
    "x": 3,
    "y": 3,
    "sorted": 15,
    "color": "rgb(253,193,193)"
  },

  {
    "x": 4,
    "y": 3,
    "sorted": 12,
    "color": "rgb(185,164,159)"
  },


  // row 5
  {
    "x": 0,
    "y": 4,
    "sorted": 0,
    "color": "rgb(246,237,222)"
  },
  {
    "x": 1,
    "y": 4,
    "sorted": 1,
    "color": "rgb(142,131,127)"
  },
  {
    "x": 2,
    "y": 4,
    "sorted": 2,
    "color": "rgb(17,29,15)"
  },
  {
    "x": 3,
    "y": 4,
    "sorted": 3,
    "color": "rgb(60,74,59)"
  },
  {
    "x": 4,
    "y": 4,
    "sorted": 16,
    "color": "rgb(226,147,143)"
  }
];

// Add circles at the top
var pixels = d3.select("#dataviz_delay")
  .append("g")
  .attr("transform", "translate(125)")

  .selectAll("rect")
  .data(position)
  .enter()
  .append("rect")
  .attr("x", function(d) {
    return d.x * 20
  })
  .attr("y", function(d) {
    return d.y * 20 + 5
  })
  .attr("width", 15)
  .attr("height", 15)
  .attr("fill", function(d) {
    return d.color
  })
  .on("mouseover", function(d) {
    d3.select(this) // Get the element which is being hovered over
    div.transition() // Set time until styling effect appears
      .duration(500)
      .style("opacity", 1); // Set the transparency of the tooltip
    //div.html('<div>' + '<img src= ' + d.path + '>' + '</div>' +  '<br>' + '<div class="story">' +'<b>' +  d.lu_class + '</b>' + " in " + d.city + ": " + d.story + '</div>') // Display the data-driven text in the tooltip
    //div.html('<div>' + '<img src= ' + d.path + '>' + '</div>' + '<br>' + '<div class="story">' + '<b>' + d.lu_class + '</b>' + " in " + d.city + ": " + d.story + '</div>')
    div.html('<div>' + '<img src= ' + d.path + '>' + '</div>' + '<br>' + '<div class="story">' +  d.lu_class + '</div>')
      .style("left", (event.pageX + 10) + "px") // Horizontal position of the tooltip - horizontal distance from the mouse pointer
      .style("top", (event.pageY + 10) + "px"); // Vertical position of the tooltip - vertical distance from the mouse pointer
  })

  .on("mouseout", function(d) {
    d3.select(this)
    div.transition()
      .duration(500) // Set time until tooltip disappears
      .style("opacity", 0); // Hide the tooltip - reset the initial transparency of tooltip to 0 - unvisible
  })

// Animation: put them down one by one:
function triggerTransitionDelay() {
  pixels
    .transition()
    .duration(2200)
    .attr("transform", "translate(-100)")
    .attr("x", function(d) {
      return d.sorted * 10
    })
    .attr("y", 70)
    .attr("width", 10)
    .attr("height", 30);

  animateGradient();

  //.delay(function(i){return(i*10)})
}



// Add tooltip to be used to display data-driven information
var div = d3.select("body") // Select the body of the website
  .append("div") // Add the container for the tooltip content
  .attr("class", "tooltip") // Add class name to the container
  .style("opacity", 0); // Set the initial transparency of tooltip to 0 - unvisible

let labelContainer = d3.select('#dna')
  .append("div")
  .attr("id", "label-container");

var labels = ["Forest", "Farmland", "Orchards", "Industrial zones", "Roads and transport", "Residential areas", "Urban green spaces"];

for (let i = 0; i < labels.length; i++) {
  let selector = labels[i].split(' ')[0];
  let alias = labels[i]

  // Append checkbox
  let label = d3.select("#label-container")
    .append("label")
    .attr("class", "label")
    .attr("id", "label-" + selector.toLowerCase())

  label
    .append("input")
    .attr("type", "checkbox")

  label
    .append("span")
    .attr("id", selector.toLowerCase())
    .attr("class", "checkbox")
    .text(alias)

  // select all stripe sections with class name
  let selected = d3.selectAll("." + selector.toLowerCase())

  // select all legend entries with id
  let selectedLegend = d3.select("#" + selector.toLowerCase())
    //console.log(selectedLegend)
    .on("click", function() {
      //console.log("Test")
      var active = selected.active ? false : true;
      highlight = active ? true : false;
      //d3.selectAll(".section")
      //  .classed("section-selected", highlight)
      d3.selectAll("." + selector.toLowerCase())
        .classed("category-selected", highlight)
      selected.active = active;
    })
}

var cities = ["Bologna", "Florence", "Torino", "Palermo", "Milan", "Rome"];

for (let i = 0; i < cities.length; i++) {

  let selector = cities[i];

  let wrapper = d3.select('#dna')
    .append("div")
    .attr("id", function(d) {
      return "wrapper-" + selector.toLowerCase()
    })
    .attr("class", "wrapper");

  let name = wrapper
    .append("div")
    .attr("class", "city-name")
    .append("p")
    .text(selector);


  let container = wrapper
    .append("div")
    .attr("id", function(d) {
      return "container-" + selector.toLowerCase()
    })
    .attr("class", "container");


  //let scaleStart = container
  //  .append("p")
  //  .attr("class", "text-low")
  //  .text("low vitality");
  //  .append("div")
  //  .attr("class", "scale-start")


  //d3.csv("data/data.csv").then(function(data) {

  d3.csv("data/data-gradients.csv", function(data) {
    container
      .selectAll('sections')
      .data(data
        .filter(function(d) {
          return d.city == selector
        }))
      .enter()

      //.append("g")
      .append("div")
      .attr("id", function(d) {
        return d.city.toLowerCase() + "-" + d.sortedID
      })
      .attr("class", function(d) {
        return "section " + d.lu_class.toLowerCase()
      })

      //.style("opacity", 1)
      //.style('stroke', "black")
      .style("stroke-width", 0.5)
      .style('width', function(d) {
        return (d.width)
      })

      .on("mouseover", function(d) {
        d3.select(this) // Get the element which is being hovered over
        div.transition() // Set time until styling effect appears
          .duration(500)
          .style("opacity", 1); // Set the transparency of the tooltip
        //div.html('<div>' + '<img src= ' + d.path + '>' + '</div>' +  '<br>' + '<div class="story">' +'<b>' +  d.lu_class + '</b>' + " in " + d.city + ": " + d.story + '</div>') // Display the data-driven text in the tooltip
        //div.html('<div>' + '<img src= ' + d.path + '>' + '</div>' + '<br>' + '<div class="story">' + '<b>' + d.lu_class + '</b>' + " in " + d.city + ": " + d.story + '</div>')
        div.html('<div>' + '<img src= ' + d.path + '>' + '</div>' + '<br>' + '<div class="story">' +  d.lu_class + '</div>')
          .style("left", (event.pageX + 10) + "px") // Horizontal position of the tooltip - horizontal distance from the mouse pointer
          .style("top", (event.pageY + 10) + "px"); // Vertical position of the tooltip - vertical distance from the mouse pointer
      })

      .on("mouseout", function(d) {
        d3.select(this)
        div.transition()
          .duration(500) // Set time until tooltip disappears
          .style("opacity", 0); // Hide the tooltip - reset the initial transparency of tooltip to 0 - unvisible
      })


    //let scaleEnd = container
    //  .append("p")
    //  .attr("class", "text-high")
    //  .text("high vitality");
  });


};
