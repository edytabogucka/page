var swatchesContainer = d3.select('#palettes')
  .append("div")
  .attr("id", "swatches-container")
  .attr("class", "swatches-container");

var swatches = ["Roofs" ];

for (let s = 0; s < swatches.length; s++) {

  let selector = swatches[s].split(' ')[0];
  let alias = swatches[s];

  // Code for loading csv file in different d3 version
  // d3.csv("data/swatches.csv").then(function(data) {
  d3.csv("data/swatches.csv", function(data) {

    var selectedContainer = d3.select('#swatches-container')
      .append("div")
      .attr("id", "swatches-container-" + selector.toLowerCase())
      .attr("class", "swatch-row")

    selectedContainer
      .selectAll('swatches')
      .data(data
        .filter(function(d) {
          return d.class == selector
        }))
      .enter()
      .append("div")
      .attr("class", "swatch")
      .style("background-image", function(d) {
        return "url(" + d.path + ")";
      })
      .style("background-color", function(d) {
        return d.color;
      })

      .on("mouseover", function(event, d) {
        d3.select(this)
          .transition()
          .duration(500)
          .style("background-color", "transparent")
      })
      .on("mouseout", function(event, d) {
        d3.select(this)
          .transition()
          .duration(500)
          .style("background-color", function(d) {
            return d.color;
          })
      })
      .append("div")
      .attr("class", "caption")
      .text(function(d) {
        return d.city;
      });
  });
}
