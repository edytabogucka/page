// Load the first dataset once the website is ready
window.onload = function() {
  loadMap('data/data-bologna-pixelate-hsv.csv', 'Bologna');
}

// Set width and height of the SVG container
var width = 600,
  height = 660;

var grid = d3.layout.grid()
  .size([560, 560]);

// Add SVG container
var svg = d3.select("#pointilism")
  .append("svg")
  .attr("id", "pointContainer")
  .attr("preserveAspectRatio", "xMidYMid") // Preserve the aspect ratio
  .attr("viewBox", [0, 0, width, height]) // Set the position and dimension, in user space, of an SVG viewport - setting for the responsive design
  .append("g")
  .attr("transform", "translate(20,60)");

// Add legend container with node size explanations
var legend = d3.select("#pointContainer")
  .append("g")
  .attr("id", "legend")
  .attr("transform", "translate(28,50)");

d3.select("#legend")
  .append("circle")
  .attr("r", 1)
  .attr("cx", 0)
  .attr("cy", -30)
  .style("fill", "white")

d3.select("#legend")
  .append("circle")
  .attr("r", 1.5)
  .attr("cx", 30)
  .attr("cy", -30)
  .style("fill", "white")

d3.select("#legend")
  .append("circle")
  .attr("r", 2.25)
  .attr("cx", 60)
  .attr("cy", -30)
  .style("fill", "white")

d3.select("#legend")
  .append("circle")
  .attr("r", 2.95)
  .attr("cx", 90)
  .attr("cy", -30)
  .style("fill", "white")

d3.select("#legend")
  .append("circle")
  .attr("r", 3.6)
  .attr("cx", 120)
  .attr("cy", -30)
  .style("fill", "white")

d3.select("#legend")
  .append("text")
  .attr("x", -14)
  .attr("y", -10)
  .style("fill", "white")
  .style("font-size", "10px")
  .text("very low vitality")

d3.select("#legend")
  .append("text")
  .attr("x", 90)
  .attr("y", -10)
  .style("fill", "white")
  .style("font-size", "10px")
  .text("very high vitality")

var size = d3.scale.sqrt()
  .domain([0, 1, 2, 3, 4, 5])
  .range([0, 1, 1.5, 2.25, 2.75, 3.5]);

/* Sorting setup */
var sortBy = {
  id: d3.comparator()
    .order(d3.ascending, function(d) {
      return d.id;
    }),
  color: d3.comparator()
    .order(d3.descending, function(d) {
      return d.hue;
    })
    .order(d3.descending, function(d) {
      return d.size;
    })
    .order(d3.ascending, function(d) {
      return d.id;
    }),
  size: d3.comparator()
    .order(d3.descending, function(d) {
      return d.size;
    })
    .order(d3.ascending, function(d) {
      return d.hue;
    })
    .order(d3.ascending, function(d) {
      return d.id;
    })
};

/* Generate maps */
function loadMap(path, city) {
  d3.selectAll(".node").remove();
  d3.select("#mapTitle").text(city);

  d3.csv(path, function(data) {
    // console.log(data);
    data.forEach(function(d) {
      d['id'] = +d['id'];
    })

    // Sort circles upon button click
    d3.selectAll(".sort-btn")
      .on("click", function(d) {
        d3.event.preventDefault();
        data.sort(sortBy[this.dataset.sort]);
        update();
      });

    update();

    // Update location of the circle
    function update() {
      var node = svg.selectAll(".node")
        .data(grid(data), function(d) {
          return d.id;
        });
      node.enter().append("circle")
        .attr("class", function(d) {
          return "node " + d.id;
        })
        .attr("r", 1e-9)
        .attr("transform", function(d) {
          return "translate(" + d.x + "," + d.y + ")";
        })
        .style("opacity", 1)
        .style("fill", function(d) {
          return ("rgb(" + d.color_R + "," + d.color_G + "," + d.color_B + ")")
        });
      node
        .on("mouseover", function(d) {
          d3.select(this)
            .style("cursor", "pointer")
            .transition()
            .duration('200')
            .attr("stroke", "black")
            .attr("stroke-width", 1)

          div.transition() // Set time until styling effect appears
            .duration(500)
            .style("opacity", 1); // Set the transparency of the tooltip
          div.html('<div class="overlay">' + '<img src= ' + d.path + '>' + '</div>' + '<br>' + '<div class="story">' +  d.lu_class + ' vitality' + '</div>') // Display the data-driven text in the tooltip
            .style("left", (event.pageX + 10) + "px") // Horizontal position of the tooltip - horizontal distance from the mouse pointer
            .style("top", (event.pageY + 10) + "px"); // Vertical position of the tooltip - vertical distance from the mouse pointer
        })

        .on("mouseout", function(d) {
          d3.select(this)
            .transition()
            .duration('100')
            .attr("r", function(d) {
              return size(d.size);
            })
            .attr("stroke", "none")
            .attr("stroke-width", 0)
          div.transition()
            .duration(100) // Set time until tooltip disappears
            .style("opacity", 0); // Hide the tooltip - reset the initial transparency of tooltip to 0 - unvisible
        })
      node.transition().duration(400).delay(function(d, i) {
          return i * 0.7;
        })
        .attr("r", function(d) {
          return size(d.size);
        })
        .attr("transform", function(d) {
          return "translate(" + d.x + "," + d.y + ")";
        });
      node.exit().transition()
        .attr("r", 1e-9)
        .remove();
    }
  });
}
