// var projectionHex = d3.geoAlbers()
var projectionHex = d3.geoAlbers()
  .center([5, 51.5])
  .rotate([-1, 0])
  //.parallels([50, 65])
  .scale(4800)

var projection = d3.geoEquirectangular()
  .scale(3500) // the zoom level of
  // .rotate([-4.64,0]) //
  .center([7, 52.2]); // move down-up
//.translate([width/2, height/2]); // dragging the center of the map

var pathHex = d3.geoPath()
  .projection(projectionHex);

d3.json("data/labels_choropleth.geojson").then(data => {
  t.selectAll("labels")
    .data(data.features)
    .enter()
    .append("text")

    .attr("x", function(d) {
      return pathHex.centroid(d)[0]
    })
    .attr("y", function(d) {
      return pathHex.centroid(d)[1]
    })
    .attr("class", "labels")
    .text(function(d) {
      return d.properties.State
    })
    .attr("transform", "rotate(6)")
    .attr("text-anchor", "middle")
    .attr("alignment-baseline", "central")
    .style("font-size", 10)
    .style("pointer-events", "none")
    .style("fill", "black")
});


d3.json("data/municipalities_choropleth.geojson").then(data => {
  l1.selectAll("municipalities")
    .data(data.features)
    //.data(topojson.feature(data, data.objects.municipalities_subset).features)
    .join("path")
    .attr("d", pathHex)
    .attr("transform", "rotate(6)")
    .attr("class", "municipalities")
    .attr("fill", function(d, i) { // Initiate the first thematic map

      var colorScale = d3.scaleThreshold() // assign fixed classes based on the classification used in the paper
        .domain([0.37, 0.47, 0.54, 0.61, 1]) // set the ends of the classes
        //.range(d3.schemeYlOrBr[5]); // set 5 classes
        .range(['rgb(255, 237, 160)', 'rgb(254, 178, 76)', 'rgb(252,78,42)', 'rgb(189,0,38)', "rgb(112,0,28)"]) // set 5 classes

      if (d.properties.NSDAP33 == "keine Daten") {
        return "white";
      } else {
        return colorScale(d.properties.NSDAP33);
      }
    })
    .attr("stroke", "white")
    .attr("vector-effect", "non-scaling-stroke")
    .attr("stroke-width", 0.15)
    .on("mouseover", function(event, d) {
      //const e = hexagons.nodes();
      //const i = e.indexOf(this);
      //console.log(d.properties.Gemeindename);
      d3.select(this)
        .style("cursor", "pointer")
        .raise() // display the current municipality on top of others
        .attr("class", "municipalities hover");
      div.transition()
        .duration(120) // time until tooltip appearss
        .style("opacity", .9); // transparency of the tooltip
      div.html(
          '<div class="tooltip-title">' + d.properties.Gemeindename + '</div>' + '<div class="tooltip-subtitle">' + d.properties.State + '</div>' + '<hr>' + 'NSDAP 1933  ' + '<b>' + d.properties.label_33 + '</b>' + "<br>" + 'AfD 2017  ' + "<b>" + d.properties.label_17 + "</b>" + "<br>" + ' AfD 2021  ' + "<b>" + d.properties.label_21 + "</b>"
        ) // display the name of the municipality
        .style("font-size", "1.25rem")
        .style("left", (event.pageX + 10) + "px")
        .style("top", (event.pageY - 10) + "px");
    })
    .on("mouseout", function(d) {
      d3.select(this)
        .lower()
        .attr("class", "municipalities");
      div.transition()
        .duration(500)
        .style("opacity", 0);
    })
    .on("click", function(event, d) {
      d3.select(this)
        .attr("z-index", "10")

      d.clicked = !d.clicked; // check if clicked

      let bounds = this.getBBox();
      //console.log(bounds)
      let x0 = bounds.x;
      let x1 = bounds.x + bounds.width;
      let y0 = bounds.y;
      let y1 = bounds.y + bounds.height;

      let scale = (1 / Math.max((x1 - x0) / width, (y1 - y0) / height)) - 20
      // zoom and center map view around the clicked polygon
      g.transition()
        .duration(1500)
        .attr("transform", d.clicked ? "translate(" + (width / 2) + "," + (height / 2) + ") scale(" + scale + ") translate(" + (-(x0 + x1) / 2) + "," + (-(y0 + y1) / 2) + ") rotate(-6)" : "transform(0,0) scale(1)");

      d3.selectAll(".labels")
        .style("font-size", 3)
    })
});

d3.json("data/borders_choropleth.geojson").then(data => {
  l2.selectAll("borders")
    .data(data.features)
    .join("path")
    .attr("d", pathHex)
    .raise()
    .attr("class", "bordersChoropleth")
    .attr("transform", "rotate(6)")
    .attr("stroke", "black")
    .attr("stroke-width", 1)
    .attr("fill", "none")
});
