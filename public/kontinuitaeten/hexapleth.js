// Define the geographical projection for the map and set the initial zoom to show the map features.
var projection = d3.geoEquirectangular()
  .scale(3500) // the zoom level of
  // .rotate([-4.64,0]) //
  .center([7, 52.2]); // move down-up
//.translate([width/2, height/2]); // dragging the center of the map

// Prepare a path object and apply the projection to it.
var path = d3.geoPath()
  .projection(projection);

// Load the features from the GeoJSON to the <g> element
var hexagons = h.selectAll("path") // Specifics of D3 - select the (non-existing) path objects first
  .data(hexagons.features) // Enter the data from the Hexagons.geojson file
  .enter()
  .append("path") // For each feature, a <path> element is added
  .attr("d", path) // The "d" attribute defines data binded to the element and the successive coordinates of the points through which the path has to go
  .attr("class", "hexagons")
  .attr("fill", function(d, i) { // Initiate the first thematic map

    var colorScale = d3.scaleThreshold() // assign fixed classes based on the classification used in the paper
      .domain([0.37, 0.47, 0.54, 0.61, 1]) // set the ends of the classes
      //.range(d3.schemeYlOrBr[5]); // set 5 classes
      .range(['rgb(255, 237, 160)', 'rgb(254, 178, 76)', 'rgb(252,78,42)', 'rgb(189,0,38)', "rgb(112,0,28)"]) // set 5 classes

    if (d.properties.NSDAP33 == "keine Daten") {
      return "white";
    } else {
      return colorScale(d.properties.NSDAP33);
    }
  })
  .attr("stroke", "white")
  .attr("vector-effect", "non-scaling-stroke")
  .attr("stroke-width", 0.25);

hexagons
  // assign on-click behaviour to the hexagons
  .on("click", function(event, d) {
    d3.select(this)
      .attr("z-index", "10")

    d.clicked = !d.clicked; // check if clicked

    let bounds = this.getBBox();
    let x0 = bounds.x;
    let x1 = bounds.x + bounds.width;
    let y0 = bounds.y;
    let y1 = bounds.y + bounds.height;
    //let scale = 1 / Math.max((x1 - x0) / width, (y1 - y0) / height); // zoom to the exact extent of the hexagon
    let scale = 10;
    // zoom and center map view around the clicked polygon
    g.transition()
      .duration(1500)
      .attr("transform", d.clicked ? "translate(" + (width / 2) + "," + (height / 2) + ") scale(" + scale + ") translate(" + (-(x0 + x1) / 2) + "," + (-(y0 + y1) / 2) + ")" : "transform(0,0) scale(1)");

    d3.selectAll(".labels")
      .style("font-size", 3)
  })
  // assign on-hover behaviour to the hexagons
  .on("mouseover", function(event, d) {
    //const e = hexagons.nodes();
    //const i = e.indexOf(this);
    //console.log(d.properties.Gemeindename);
    d3.select(this)
      .style("cursor", "pointer")
      .raise() // display the current hexagon on top of others
      .attr("class", "hexagons hover");
    div.transition()
      .duration(120) // time until tooltip appearss
      .style("opacity", .9); // transparency of the tooltip
    div.html(
        '<div class="tooltip-title">' + d.properties.Gemeindename + '</div>' + '<div class="tooltip-subtitle">' + d.properties.State + '</div>' + '<hr>' + 'NSDAP 1933  ' + '<b>' + d.properties.label_33 + '</b>' + "<br>" + 'AfD 2017  ' + "<b>" + d.properties.label_17 + "</b>" + "<br>" + ' AfD 2021  ' + "<b>" + d.properties.label_21 + "</b>"
      ) // display the name of the municipality
      .style("font-size", "1.25rem")
      .style("left", (event.pageX + 10) + "px")
      .style("top", (event.pageY - 10) + "px");
  })
  .on("mouseout", function(d) {
    d3.select(this)
      .lower()
      .attr("class", "hexagons");
    div.transition()
      .duration(500)
      .style("opacity", 0);
  });


// Add data with cartogram borders
var borders = h.selectAll("borders")
  .data(borders.features)
  .enter()
  .append("path")
  .attr("d", path)
  .attr("stroke", "black")
  .attr("stroke-width", 1)
  .attr("fill", "none");

// Add data with state labels
var labels = h.selectAll("labels")
  .data(labels.features)
  .enter()
  .append("text")
  .attr("x", function(d) {
    return path.centroid(d)[0]
  })
  .attr("y", function(d) {
    return path.centroid(d)[1]
  })
  .attr("class", "labels")
  .text(function(d) {
    return d.properties.State
  })
  .attr("text-anchor", "middle")
  .attr("alignment-baseline", "central")
  .style("font-size", 10)
  .style("pointer-events", "none")
  .style("fill", "black");

// Create an empty array to store the names of municipalities
var list = [];

d3.selectAll(".hexagons")
  .filter(function(d) {
    list.push(d.properties.Gemeindename); // append the names of the municipalities to the array
  });

list.sort(); // sort list alfabetically

// Add the names to the selection list
d3.select('datalist').selectAll('option')
  .data(list) // performing a data join
  .enter() // extracting the entering selection
  .append('option') // adding an option to the selection of municipalities
  .attr('value', function(d) {
    return d
  }); // add attribute with municipality name
