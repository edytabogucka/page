// Global variables
var _vis;

// Function setup for the visualization environment
function setupViz() {
  _vis = new FlowerVis();
  _vis.svgContainer = d3.select("#vis");
  loadData("states.csv");
}

// Load data from a given CSV file path/url
function loadData(path) {
  // call D3's loading function for CSV and load the data to our global variable _data
  d3.csv(path).then(function(data) {
    _vis.data = data;
    _vis.createFlowers();
  });
}

var FlowerVis = function(data) {
  this.data = data;
  this.width = 1500;
  this.height = 750;
  this.svgContainer;

  this.FLOWER_WIDTH = 150;
  this.FLOWER_HEIGHT = 150;
  this.flowers; // groups of svg containing svg shapes that form a "flower"

  // Values below are now hard-coded in the html file
  //d3.select("#vis")
  //  .attr("preserveAspectRatio", "xMidYMid") // Preserve the aspect ratio
  //  .attr("viewBox", [0, 0, this.width, this.height]);

  // Load SVG file with US borders
  let states = d3.xml("states.svg")
    .then(data => {
      d3.select("#vis").node().append(data.documentElement)
    });

  // Move the position of flowers from grid to map
  function drawMap() {
    d3.selectAll(".flower_group")
      .attr("transform", function(d) {
        return "translate(" + d["xmap"] + "," + d["ymap"] + ") scale(0.75)";
        //return "translate(" + d["xmap"] + "," + d["ymap"] + ")";
      })
    d3.select("#states")
      .style("opacity", 0.3)
  }

  // Move the position of flowers from map to grid
  function drawGrid() {
    let this_group = d3.selectAll(".flower_group")
      .attr("transform", function(d) {
        return "translate(" + d["xgrid"] + "," + d["ygrid"] + ") scale(1)";
        //return "translate(" + d["xgrid"] + "," + d["ygrid"] + ")";
      })
    d3.select("#states")
      .style("opacity", 0)
  }

  // Assign the drawing functions to buttons
  let map = d3.select("#mapButton")
    .on("click", drawMap);

  let grid = d3.select("#gridButton")
    .on("click", drawGrid);

  // Create flower group holding inside petals and stems
  this.createFlowers = function() {
    let _this = this;

    this.flowers = this.svgContainer.selectAll("g")
      .data(this.data)
      .enter()
      .append("g")
      .attr("width", this.FLOWER_WIDTH)
      .attr("height", this.FLOWER_HEIGHT)
      .attr("transform", function(d) {
        return "translate(" + d["xgrid"] + "," + d["ygrid"] + ")";
      })
      .attr("class", function(d) {
        return "flower_group" + " group_" + d["Timestamp"]
      });

    // Add petals to the flower
    this.addPetal(120, "Refusal", 4, "lightskyblue", "white");
    this.addPetal(200, "Anger", 4, "Red", "white");
    this.addPetal(260, "Death", 15, "AliceBlue", "white");
    this.addPetal(320, "Acceptance", 4, "Aquamarine", "white");

    // When the input range changes update the petal size
    d3.select("#date").on("input", function() {
      update(+this.value);
      //console.log(+this.value)
    });

    // Initial starting size of the petal - day 1
    update(1);

    // Update the petals upon date selection
    function update(date) {
      d3.select("#vis")
        .selectAll(".flower_group")
        .attr("display", "none"); // Swicth off the flower group

      d3.select("#vis")
        .selectAll(".group_" + date)
        .attr("display", "block"); // Switch on the flower group related to time input

      d3.select("#vis")
        .selectAll(".flower_petal")
        .attr("visibility", "hidden");

      d3.select("#vis")
        .selectAll(".petal_" + date)
        .attr("visibility", "visible");

      // Animate gradient line
      d3.select("#gradient-path")
        .transition()
        .delay(11000)
        .ease(d3.easeLinear)
        //.attr("stroke-dashoffset", 2462 - date * 10)
        .attr("stroke-dashoffset", 0)
        .duration(6000)
      }

    // Add label with the country code to the group container
    let label = d3.selectAll(".flower_group")
      .append("text")
      .text(function(d) {
        return d["Code"];
      })
      .attr('font-size', "18px")
      .style("fill", "white")
      .attr('dx', 75) //positions text towards the left of the center of the circle
      .attr('dy', 150);

    // Add stem path
    this.flowers.append("path")
      .attr("d", function(d) {
        let path = "";
        let origin = {
          x: _this.FLOWER_WIDTH / 2,
          y: _this.FLOWER_HEIGHT / 2
        };
        let top = {
          x: _this.FLOWER_WIDTH * 0.75,
          y: _this.FLOWER_HEIGHT
        };
        let ctrl1 = {
          x: _this.FLOWER_WIDTH / 2,
          y: _this.FLOWER_HEIGHT * 0.75
        };
        let ctrl2 = {
          x: _this.FLOWER_WIDTH * 0.75,
          y: _this.FLOWER_HEIGHT * 0.75
        };
        path = `M${origin.x} ${origin.y}
                  C ${ctrl1.x} ${ctrl1.y}
                  ${ctrl2.x} ${ctrl2.y}
                  ${top.x} ${top.y} `;
        return path;
      })
      .style("stroke-width", 1)
      .style("stroke", "white")
      .style("fill", "none")
      .style("opacity", 0)
      .transition()
      .style("opacity", 1);

    // Add flower head
    let flower_head = this.flowers.append("circle")
      .style("fill", "white")
      .attr("cx", this.FLOWER_WIDTH / 2)
      .attr("cy", this.FLOWER_HEIGHT / 2)

    // Display state name once the mouse is over the head
    flower_head
      .on("mouseover", function() {
        d3.select(this)
          .transition(400)
          .style("stroke-width", 15)
          .style("stroke", "#001c75");
      })
      .on("mouseout", function() {
        d3.select(this).style("stroke", "none");
      })
      .append("svg:title")
      .text(function(d) {
        return d["State"];
      });

    flower_head
      .attr("r", 0)
      .transition(400)
      .attr("r", 10);
  }

  // Function to add data-driven petal
  this.addPetal = function(rotation_angle, selector, max_val, fill, stroke) {
    let _this = this;

    let petal_group = this.flowers.append("g")
      .attr("transform", `rotate(${rotation_angle}, ${_this.FLOWER_WIDTH/2}, ${_this.FLOWER_HEIGHT/2})`)
      .attr("class", "flower_stem");

    // Add central dash to the stem
    let dash = petal_group.append("line")
      .attr("x1", _this.FLOWER_WIDTH / 2)
      .attr("y1", _this.FLOWER_HEIGHT / 2)
      .attr("x2", 100)
      .attr("y2", 100)
      .style("stroke-width", 1)
      .style("stroke", "white")
      //.attr("class", function(d) {
      //  return "stem_" + d["Timestamp"]
      //})
      .style("opacity", 1);

    // Add left dash to the stem
    let dash_left = petal_group.append("line")
      .attr("x1", 100)
      .attr("y1", 100) // spread
      .attr("transform", "translate(-10, -10)")
      .attr("x2", _this.FLOWER_WIDTH / 2 + 40) // length
      .attr("y2", _this.FLOWER_HEIGHT / 2 + 30) // spread
      .style("stroke-width", 1)
      //.attr("class", function(d) {
      //  return "stem_" + d["Timestamp"]
      //})
      .style("stroke", "white")
      .style("opacity", 1);

    // Add right dash to the stem
    let dash_right = petal_group.append("line")
      .attr("x2", 120)
      .attr("y2", 120) // spread
      .attr("transform", "translate(-25, -15)")
      .attr("x1", _this.FLOWER_WIDTH / 2 + 40) // length
      .attr("y1", _this.FLOWER_HEIGHT / 2 + 30) // spread
      .style("stroke-width", 1)
      .style("stroke", "white")
      //.attr("class", function(d) {
      //  return "stem_" + d["Timestamp"]
      //})
      .style("opacity", 1);

    // Add petal to the stem
    let petal = petal_group.append("circle")
      .attr("class", function(d) {
        return "flower_petal " + "petal_" + d["Timestamp"]
      })
      .attr("selector", selector)
      .attr('cx', 100)
      .attr('cy', 100)
      .attr("visibility", "hidden")
      .attr('r', function(d) {
        // Set the minimum and maximum size of the petal
        let scale = d3.scaleLinear()
          .domain([0, max_val])
          .range([10, 24])
        return scale(d[selector])
      })
      .attr('stroke', 'none')
      .style("opacity", 0.1)
      .style("fill", fill);

    petal
      .transition()
      .style("opacity", 1)
      .style("fill-opacity", 0.75)
      .duration(800);

    petal
      .on("mouseover", function(event, d) {
        d3.select(this)
          .style("stroke-width", 1)
          .style("cursor", "pointer")
          .style("stroke", stroke);

        d3.selectAll(".flower_petal")
          .filter(function(e) {
            return d3.select(this).attr("selector") !== selector;
          })
          .transition()
          .style("opacity", 0.1);
      })
      .on("mouseout", function() {
        d3.select(this)
          .style("stroke", 'white')
          .style("stroke-width", 0);

        d3.selectAll(".flower_petal")
          .transition()
          .style("opacity", 1);
      })
      .append("svg:title")
      .text(function(d) {
        return `${selector}: ${d[selector]}`;
      });
  }
};
