// Call functions on window load event
window.onload = function() {
  setTimeout(function() {
    $('#preloader').fadeOut('slow')
  }, 13000);
  setupViz();
  parseDate();
  drawGradient();
}

function parseDate() {
  // Parse date in YYYY-MM-DD format as local date
  function parseISOLocal(s) {
    let [y, m, d] = s.split('-');
    return new Date(y, m - 1, d);
  }

  // Format date as YYYY-MM-DD
  function dateToISOLocal(date) {
    let z = n => ('0' + n).slice(-2);
    return date.getFullYear() + '-' + z(date.getMonth() + 1) + '-' + z(date.getDate());
  }

  // Convert range slider value to date string
  function range2date(evt) {
    let dateInput = document.querySelector('#date-helper');
    let minDate = parseISOLocal(dateInput.defaultValue);
    minDate.setDate(minDate.getDate() + Number(this.value));
    dateInput.value = dateToISOLocal(minDate);
    let dateDisplay = document.querySelector('#date-value');
    dateDisplay.textContent = dateInput.value;
    //console.log(dateDisplay)
  }

  // Convert entered date to range
  function date2range(evt) {
    let date = parseISOLocal(this.value);
    let numDays = (date - new Date(this.min)) / 8.64e7;
    document.querySelector('#date').value = numDays;
  }

  let rangeInput = document.querySelector('#date');
  let dateInput = document.querySelector('#date-helper');
  // Get the number of days from the date min and max
  // Dates in YYYY-MM-DD format are treated as UTC
  // so will be exact whole days
  let rangeMax = (new Date(dateInput.max) - new Date(dateInput.min)) / 8.64e7;
  // Set the range min and max values
  rangeInput.min = 1;
  rangeInput.max = rangeMax;
  // Add listener to set the date input value based on the slider
  rangeInput.addEventListener('input', range2date, false);
  // Add listener to set the range input value based on the date
  dateInput.addEventListener('change', date2range, false);

}

function drawGradient() {
  //console.log("we will draw")
  const svg = d3.create("svg")
    .attr("preserveAspectRatio", "xMidYMid") // Preserve the aspect ratio
    .attr("viewBox", [0, 0, 1500, 150]); // Set the dimensions for the gradient SVG file

  // Append gradient line
  var line = svg.append("path")
    .attr("d", "M12.8,82.945c0,0,4.3,5.459,4.3,7.537s4.4,4.244,4.4,4.244l3.5,0.78  l1.7,2.252l1.898,7.362l2.4,4.505l1.7,3.726l1.698,3.378l6.2,1.906l3.2,1.472l5.8,2.859l4.7,1.904l2.6,1.388l4.4-0.261l3.7,0.261  l0.8,0.086l4.4-0.26l7.6,0.174l2.6,0.174l6.7-0.435l4.4,0.605l4.398-0.691l3.602,1.299l5.3-1.646l3.6-2.079l2.4-4.158l1.9-3.726  l1.6-5.544l2.1-10.48l3.102-16.979l4.3-23.39l3.9-22.522l2.1-11.521l1.6-8.489l1.5-4.938l1.9-5.111l2.3-0.433l1.9,3.378l1.3,2.858  l1.5,7.796l2.6,14.12l1.7,11.868l1.3,12.388L149,70.904l2,10.049l1.7,8.924l1,5.023l4.398,4.418l4.4-11.607l4.398-2.254l3.302-0.172  l1.1-0.088l8.9,2.859l7.3,2.225l1.5,5.053l4.398,5.805l4.4-2.08l2.034-1.27l2.367-1.242l4.898,0.173l3.8,1.646l0.602,0.174  l4.398-2.687l4.4-1.126l4.398-2.166l4.4-2.252l4.4-9.009l4.398-3.897l4.4-0.606l4.398-4.244l4.4,1.386l4.4,27.201l4.398,1.905  l4.4,10.655l4.398,4.504l4.4-1.474l4.399-1.731l4.399-2.6l4.4,0.779l4.398,1.387l4.4,3.898l0.399,1.645l4.399,3.119l3.2,0.52  l3.3,1.041l2.399,0.52l2,0.866l2.399,1.213l2.899,1.04l2,0.346l2.899,0.521l3-0.088l2.601,0.088l4.399,0.865l3.3-0.779l2.602-0.606  l3-0.434l3.699-0.778l3-0.088l2.2-0.086l4.399,0.779l8.899-7.883l4.399-0.867l4.4,3.553l4.398-5.63l4.4-3.379l5.1,6.409l3.801,4.504  l4.399,3.207l4.4,0.52l4.898-2.079l4-0.78l4.4-1.817l4.399,0.433l4.399,2.426l4.399,2.772l4,1.992l3.301,0.521h3.699h2.301  l2.8,0.086l2.101,0.086l2.3,0.262l3.399-0.606l5.4-1.126l3-0.261l3.699-0.261l3.101,0.261l5.101,0.866l3.699,0.261l2.601-0.261  l7.301-1.473l6-1.906l4.399-2.078l4.399-3.553l3-3.638l2.101-2.252l3.3-2.772l0.899-1.646l1.601-5.978l2.5-8.662l1.2-7.363  l1.601-9.009l3-17.239l2.1-13.513l0.9-4.591l1.899-8.922l1.899-8.143l0.7-2.599l4.399,0.52l1.199,4.071l3.2,9.183l2.2,12.821  l2.3,12.387l1.601,14.554l2.399,23.043l1.3,7.623l2.301,10.049l3.199,5.285l7.5,1.299l2.2-0.174l5.5-0.953l3.3-3.205l2.5-15.246  l3.4-20.876l3-15.939l4.399-11.088l4.399-2.425l4.301,13.688l1.899,10.049l2.101,11.867l2.3,8.75l2.399,8.488l0.301,1.3l4.399,7.363  l4,0.952l2.601,1.732l2.399,1.473l3.8,0.779l5.101-1.56l3.8,0.866l3.9,1.386l1.199,0.692l5.301,0.348l4-0.261l4.899-0.347  l3.601-0.26l4.899,1.732l4,0.173l4-0.866l2-0.087l3.399-0.606h3.899l2,0.606l2.5,0.866l3.5-1.732l2.301-1.646l3.1-1.992l4.4,2.687  l4.399,2.857l5,0.952l1,0.174l2.399,0.521l5,0.605l4.4-2.771l3-2.6l1.5-1.386l3.1-1.299l2.5-0.693l3.3-0.953l4.4,1.646l2.3,2.599  l2.101,2.339l4,2.772l3.399,1.039l4.601,1.473l3.101,0.605l3.6,0.434l2.5-0.086l3.601-0.087l3.801-0.347l2.5-0.348l3.6-1.904  l1.2-0.435l5.2-1.212l2.899-0.693l3.399-1.732l3.7-1.732l2.399-0.951l1.899-0.521l2.2-0.606l2.399,0.346l2.101,0.348l2.6,1.213  l2.101,1.039l2.5,1.82l1.7,1.126l2,0.778l2.101,0.953l3.3,0.779l1.5,0.348l3.2,0.086h1.3l3.3-0.953l1.101-0.26l4.399-2.426  l2.4-0.087h2l1.699,0.261l2.5,0.346l3.399,0.78l1.7,0.347l2.7,0.433l1.3,0.349l2.7-0.175l1.8-0.174l1.3,0.261l2.101,0.348  l1.101,0.086l2.3,1.213l2.101,1.213l3.899,0.605l1.9,0.088l2.5-0.088l2.3-0.26l2.399-0.347h0.399l1.801-0.692l2.601-0.953l2.5,0.346  l3.3,0.262l3.8,0.086h3.801l4.399-0.52l4.399-0.174l2.7,0.866l1.399,0.778l3.7,0.434l2.1,0.434l4.4,0.088l3.1-0.174l2.4-0.347  l2.5-0.26l3.899-1.387l5-2.165h2.5l1.899,0.259l3.5-0.778l1.4-0.26l1.3-0.779l2.7-1.474l1.5-0.521l2.398-0.779l1.9-0.26l2.6-0.174  l2.7,0.348l1.8,0.52l0.9,0.174l2.3,1.56l1.8,0.953l1.301,0.52l1.301,0.347l2.398,0.953l2.301,0.606l1.301,0.433l1.3-0.605l2-1.039  l2.899-1.475l2.301-1.037l0.8-0.088l2.1-0.261l2.7-0.173l2-0.693l2-0.605l1.3-0.605l1.2-1.646l1.3-2.6l2.102-0.953l2.3,0.781  l1.2,2.252l1.6,3.726l1.3,2.425l0.4,0.779l1.1,0.433l1.601,0.867l1.7,0.866l2.199-1.561l1.5-1.473l1.301-0.779l2.5-0.953l1.5-0.865  l1.399-1.387l2.101-1.56l1.8-0.953l3.5-1.472l2.601,1.817l1.8,1.56l1.5,1.992l1.7,2.254l1.601,1.386l2.5,1.559l1.6,1.214  l4.101,0.952l3.7,0.087l2.5-1.039l2.5-1.3l2.199-1.56l2.2-1.992l1.699-1.04l2.7-1.039l4.399,6.063l4.399-2.079l3.301-5.978  l2.5-4.505l3.101-5.11l2.5-8.402l1.899-5.979l1-5.371l1.601-9.354l1-6.063l1.899-11.867l1.5-9.183l0.601-5.631l0.6-2.08v-1.132  l0.9-0.346l1.1,3.725l1.5,6.93l1.601,7.45l0.699,6.063l3.4,29.453v3.031l2.399,8.489l2.399,7.537l4.4,5.804l4.399,2.858  l11.168-2.312l9.531-6.092l1.301-3.206l2.601-6.235l3.5-3.553l4.399-0.865l4.399,6.844l4.399,4.244l7.899,1.731l0.9,0.347  l4.399,1.906l3,0.779l4.7,1.904l3.699,1.474l4.101,1.905l4.5,0.779l2.3,0.434l2.801-1.038l3-1.214l3.8-0.866l4.2-0.866l4.1-0.779  l4.4,0.088l1.6,0.605l2.9,1.126l3.699-0.434l4.7-0.347l1.5-0.086l4.199,0.086l1.9-0.174l1.8-0.173l3.301,0.261l2.199,0.605  l2.899,0.865l1.899,0.174l3.101,0.173l2-1.212l3.5-2.079l3.399-1.992l4.4-0.087l4.399-1.818l2.7-0.692l1.8-0.607l2.101,0.521  l2.399,0.865l2.301,1.82l1.8,1.559l2,1.127l2.801,1.731l3.5-2.339l1-0.691l2.5-3.207l2-2.338l4.399-3.119l4.399,1.301l4.399,3.205  l4.4,3.639l3.6,2.6l0.801,0.605l4.399,1.646l4.399-1.213l3.601-2.34l2.5-1.386l2.801-1.819l4.399-1.906l4.399-3.291l4.399-1.646  l4.4,2.339l4.399,1.906l4.399,8.488l4.4-1.646l4.1-4.852l1.9-1.561l2.899-2.599l2.101-2.945l2.399-3.203l4.399-4.593l4.101,2.687  l0.5,0.691l1.899,4.418l2,4.938l4.7,7.276l4.398,1.127l4.4-0.261l3.399,1.818l2.301,1.474l2.699,1.818l0.601,0.434l3,0.434  l1.899,0.347h2.5l3-0.086l3-0.087l4.399,0.691l4-0.865l0.5-0.174l2.101-1.904l2.399-1.646l2.7-1.992l1.8-1.213l1.9-1.126l2.5-1.213  l2.699-2.599l2.101-1.993l1.899-3.204l2.101-3.205l4.399-3.726l4.399,0.605l3.4,2.771l5.5,5.024l4.399-1.906l3.899,2.427  l1.101,0.606l3.899,3.465l4.899-2.426l4-1.646l1.2-0.693l3.3-2.165")
    .attr("stroke", "white")
    .attr("id", "gradient-path")
    .style("opacity", 1)
    //.attr("transform", "translate(0, -40)")
    .attr("fill", "none");

  const length = line.node().getTotalLength();
  //console.log(length)

  line
    .attr("stroke-dasharray", length + " " + length)
    .attr("stroke-dashoffset", length);

  // Append label to the gradient
  svg
    .append("text")
    .attr("x", 0)
    .attr("y", 45)
    .attr("font-size", "18px")
    .style("fill", "white")
    .text("language");
  svg
    .append("text")
    .attr("x", 0)
    .attr("y", 65)
    .attr("font-size", "18px")
    .style("fill", "white")
    .text("variation");

  d3
    .select("#gradient")
    .append(() => svg.node());
}
