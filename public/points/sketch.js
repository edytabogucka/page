let img; // initialize variable

function preload() {
  img = loadImage('Hannover.jpg'); // preload the image
}

function setup() {
  let canvas = createCanvas(1024, 1024); // set canvas size the same as image size
  canvas.parent('sketch-container');
  // image(img,0,0); // draw an image to the p5.js canvas
  imageMode(CENTER);
  noStroke();
  img.loadPixels(); // loads the pixel data for the display window into the pixels[] array.
}

function draw() {
  frameRate(10); // speed of drawing
  let x = floor(random(img.width)); // randomly select the x position of the point
  let y = floor(random(img.height)); // randomly select the y position of the point
  let pix = img.get(x, y); // fetch a value (color) of a single pixel of an image
  fill(pix,128); // set the fill color to the fetched color
  rect(x, y, 40, 40); // draw a square on x, y location with the pixel color grabbed from the underlying image and specified width and height
  //ellipse(x, y, 30, 30); // draw a point on x, y location with the pixel color grabbed from the underlying image
  stroke(color(255, 255, 255)); // set circle color to white
  strokeWeight(4);
}
